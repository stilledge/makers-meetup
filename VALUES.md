# Values

These are our values!

**Creativity**
- Anything creative is encouraged. Certainly not limited to tech.
- Creativity doesn’t mean unique. Don’t feel pressure to come up with novel ideas.

**Iteration**
- Please share your **roughest** draft.
- Let’s encourage **tangible** progress and discourage over-engineering.
- Everything about the Maker’s Meetup is in draft. Even these values.

**Collaboration**
- Let's be encouraging, respectful, and open-minded.
