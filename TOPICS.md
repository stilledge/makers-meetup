# Agenda Topics

Here's some example agenda topics!

**Project Update**

Show us what you've been working on! 

In the agenda, please include the name and a short description of 
your project. Let's try to keep this inclusive to everyone. 
*EXAMPLE:*

- Alice: Project update for Toaster Bonanza.
  - A virtual reality experience.
  - https://example.com/my/demo

**Collaborate**

Have a specific or general question you'd like to discuss? Ask it! 
*EXAMPLE:*

- Bob: Hey everyone! I’m having a hard time finding a cool idea to work on… What do you think I should do?

**Meta**

Do you have an idea to help make this meetup awesome? Share it! 
*EXAMPLE:*

- Mary S.: Let’s get meta! This is great, but what if we also added some aerobics?
