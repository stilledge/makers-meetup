# Makers Meetup

A supportive environment for creatives to share and collaborate on their passion projects.

## Agenda

Interested in participating? Checkout the [public agenda document][1] and add your topic to next meetup's agenda. 

## What are some example topics?

Check out our [topics document](./TOPICS.md)!

## What values do you have?

Check out our [values document](./VALUES.md)!

[1]: https://docs.google.com/document/d/1RM-4cbJiVMdtbnTRVxE-nEEGFqxA8K8CBcJY7XOXLRE/edit
